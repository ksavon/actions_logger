$LOAD_PATH.unshift 'lib'
require 'actions_logger/version'

Gem::Specification.new do |s|
  s.name        = 'actions_logger'
  s.version     = ActionsLogger::VERSION
  s.date        = '2016-10-05'
  s.summary     = "Logging"
  s.description = "Logging requests among servises"
  s.authors     = ["Konstantin Savon"]
  s.email       = 'savon@1pt.com'

  s.add_dependency 'activerecord','>= 3.2.0', '< 4.3'
  s.add_dependency 'actionpack','>= 3.2.0', '< 4.3'
  s.add_dependency "ruby-prof", '>=0.15.9'
  s.add_dependency 'rest-client', '>=1.6.0', '<= 2.0'
  s.add_dependency 'sqlite3', '> 1.3'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end