require 'socket'
server = TCPServer.new 2000 # Server bound to port 2000

filename = File.expand_path '../log/development.log', __FILE__

file = File.open(filename, 'a')
file.sync = true

loop do
  Thread.start(server.accept) do |client|
    while message = client.gets
      file.puts message
    end
  end
end
