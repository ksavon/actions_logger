ActionsLogger::Engine.routes.draw do
  resources :log_results, only: [:index, :show], path: 'results', module: 'actions_logger' do
    collection do
      get :clean
      get :reset_level
    end
  end

  if ENV["RAILS_ENV"] == 'test'
    resources :tests, only: :index
  end
end
