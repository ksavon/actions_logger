require 'actions_logger/version'
require 'actions_logger/file_name_logger'
require 'actions_logger/benchmark_filter'
require 'actions_logger/log_item'
require 'actions_logger/engine'
require 'ruby-prof'
require 'rest-client'

class RestClient::Request
  alias :old_initialize :initialize

  def initialize(args)
    action_info = ActionsLogger::ActionInfo.new do |action|
                    action.callers = caller
                    action.save
                  end

    if args[:payload].is_a?(String)
      params = JSON.load(args[:payload])
      params[:logger_action_info_id] = action_info.id if action_info.persisted?
      args[:payload] = JSON.dump(params)
    end

    ActionsLogger::BenchmarkFilter.logger.debug(args)
    old_initialize(args)
  rescue => e
    puts e.message
    raise args[:payload].inspect

  end
end

module ActionsLogger
  require 'actions_logger/engine'

  HOME_PATH = '/home/vagrant'
  GUEST_HOME = '/Users/konstantinsavon/workspace/sportech'
  LOG_FILE = '/development.log'
  EDITOR_NAME = 'subl'
  DIR_PATH = File.expand_path('../actions_logger/log')

  def self.init_directory
    FileUtils.mkdir_p(DIR_PATH) unless File.directory?(DIR_PATH)
  end

  def self.file_path
    init_directory
    DIR_PATH + LOG_FILE
  end

  def self.activate
    logfile = File.open(file_path, 'a')
    logfile.sync = true
    BenchmarkFilter.logger = ActionsLogger::FileNameLogger.new(logfile)

    ActiveSupport.on_load(:action_controller) do
      around_filter BenchmarkFilter
    end
  end
end

ActionsLogger.activate