class ActionsLogger::LogItem
  attr_accessor :id, :date, :time, :mode, :path, :action, :line, :action_info

  delegate :duration, :level, to: :action_info
  def initialize(action_info)
    @action_info = action_info
    @id = action_info.id
    @path, @action = action_info.log_line.split
    @path, @line = @path.split(':')
  end

  def path_with_line
    "#{path}:#{line}"
  end

  def editor_link
    file_path = "#{path}&line=#{line}"
    "#{ActionsLogger::EDITOR_NAME}://open?url=file://#{ActionsLogger::GUEST_HOME}#{file_path}"
  end

  def report_link
    file_path = path.sub('/', '').gsub('/','-')
    file_path << action.to_s

    URI.escape("file://#{ActionsLogger::GUEST_HOME}/actions_logger/performance/#{file_path}.html")
  end

  def show_link
    "/log/results/#{id}"
  end

  def timestamp
    "#{date} #{time}"
  end
end
