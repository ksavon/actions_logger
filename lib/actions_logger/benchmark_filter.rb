require 'active_record'
require File.expand_path '../actions_logger/app/models/actions_logger/base'
require File.expand_path '../actions_logger/app/models/actions_logger/action_info'
require File.expand_path '../actions_logger/app/models/actions_logger/request_level'

class ActionsLogger::BenchmarkFilter
  class << self
    attr_accessor :logger, :controller

    def filter(controller, &block)
      @controller = controller
      if !logger_controller? && controller.params[:logger_action_info_id].present?
        logger.debug controller.params
        info_id = controller.params.delete(:logger_action_info_id)
        last_record = ActionsLogger::ActionInfo.find_by_id(info_id)
      end

      log_current_location unless logger_controller?

      ActionsLogger::RequestLevel.level.increment(:value).save
      start_time = Time.now.to_f

      profile_if_necessary(&block)

      end_time = Time.now.to_f

      if last_record and not logger_controller?
        last_record.log_line = "#{@location} ##{controller.action_name}"
        last_record.duration = ((end_time - start_time) * 1000).to_i
        last_record.level = ActionsLogger::RequestLevel.level.value
        last_record.save
      end

      ActionsLogger::RequestLevel.level.decrement(:value).save
    end

    alias :around :filter

    def log_current_location
      @location = controller.method(controller.action_name).source_location.
                              join(':').gsub(ActionsLogger::HOME_PATH, '')
    end

    def logger_controller?
      controller.is_a?(LogResultsController)
    end

    def log_report(result)
      printer  = RubyProf::GraphHtmlPrinter.new(result)
      printer.print(File.open(file_path, 'w'), min_percent: 0)
    end

    def file_path
      dirname = init_directory
      [dirname, filename].join('/')
    end

    def init_directory
      dirname = File.expand_path "../actions_logger/performance/"
      FileUtils.mkdir_p(dirname) unless File.directory?(dirname)

      dirname
    end

    def filename
      default_name = "unknown_location #{controller.to_s.underscore}"
      file = @location.split(':').first.sub('/', '').gsub('/', '-')
      file += "##{controller.action_name}"

      (file.blank? ? default_name : file) + '.html'
    end

    private

    def profile_if_necessary(&block)
      if false
        result = RubyProf.profile { with_exception_loging(&block) }
        log_report(result) unless controller.is_a?(LogResultsController)
      else
        with_exception_loging(&block)
      end
    end

    def with_exception_loging(&block)
      yield
    rescue => e
      logger.debug e.message
      e.backtrace.each { |line| logger.debug line }
    end
  end
end
