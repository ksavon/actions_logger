class ActionsLogger::RequestLevel < ActionsLogger::Base

  def self.level
    ActionsLogger::RequestLevel.first_or_create
  end

  unless connection.table_exists?(table_name)
    connection.create_table table_name, force: true do |t|
      t.integer :value, default: 0
    end
  end
end
