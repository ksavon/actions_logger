class ActionsLogger::Base < ActiveRecord::Base
  self.abstract_class = true
  establish_connection adapter: 'sqlite3', database: File.expand_path('../../../../actions_info.db', __FILE__)
end
