class ActionsLogger::ActionInfo < ActionsLogger::Base
  serialize :callers, Array

  # if defined?(:attr_accessible)
  #   attr_accessible :callers, :log_line
  # end

  unless connection.table_exists?(table_name)
    connection.create_table table_name, force: true do |t|
      t.text :log_line
      t.text :callers
      t.integer :duration
      t.integer :level

      t.timestamps
    end
  end
end
