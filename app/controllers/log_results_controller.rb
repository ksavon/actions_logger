require 'fileutils'

class LogResultsController < ActionController::Base
  def index
    # ActionsLogger::ActionInfo.delete_all
    @log_items = ActionsLogger::ActionInfo.all.map do |action_info|
      ActionsLogger::LogItem.new(action_info) if action_info.log_line.present?
    end.compact.sort_by(&:timestamp)
  end

  def show
    @info = ActionsLogger::ActionInfo.find(params[:id])
  end

  def clean
    File.truncate(ActionsLogger.file_path, 0)
    ActionsLogger::ActionInfo.delete_all
    dirname = ActionsLogger::HOME_PATH + '/actions_logger/performance'
    FileUtils.rm_rf(Dir.glob(dirname))

    redirect_to '/log/results'
  end

  def reset_level
    ActionsLogger::RequestLevel.update_all(value: 0)
    redirect_to '/log/results'
  end

  def caller_link(caller_path)
    path, line = caller_path.sub(ActionsLogger::HOME_PATH, '').split(':')
    file_path = "#{path}&line=#{line}"
    "#{ActionsLogger::EDITOR_NAME}://open?url=file://#{ActionsLogger::GUEST_HOME}#{file_path}"
    [[path, line].join(':'), "#{ActionsLogger::EDITOR_NAME}://open?url=file://#{ActionsLogger::GUEST_HOME}#{file_path}"]
  end

  helper_method :caller_link
end
